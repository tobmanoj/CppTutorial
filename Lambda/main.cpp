#include <iostream>

using namespace std;

//auto sum = [](int x, int y) -> int
//{
//return x + y;
//};

//cout << sum(2, 3); // "5"

//Including the return type is optional if the compiler can deduce the
//return value from the lambda. In C++11, this required the lambda to
//contain just a single return statement, whereas C++14 extended return
//type deduction to any lambda function. Note that the arrow operator (->)
//is also omitted when leaving out the return type.

//auto sum = [](int x, int y) { return x + y; };

//C++11 requires lambda parameters to be declared with concrete types.
//This requirement was relaxed in C++14, allowing lambdas to use auto type
//deduction.

//auto sum = [](auto x, auto y) { return x + y; };


//Variables captured by value are normally constant, but the
//mutable specifier can be used to allow such variables to be modified.
//int a = 1, b = 1;
//[&, b]() mutable { b++; a += b; }();

//As of C++14, variables may also be initialized inside the capture clause.
//If there is no variable with the same name in the outer scope, the variable
//will be created and its type deduced as if declared with auto.
//int a = 1;
//[&, b = 2]() { a += b; }();
//cout << a; // "3"

int main()
{
    cout << "Lambda Function" << endl;
    return 0;
}
