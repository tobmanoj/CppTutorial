#include <iostream>

using namespace std;

class MyClass {
public:
    MyClass(){}
    ~MyClass(){
        cout << "Destructor";
    }

    operator int() { return 1; } // Conversion operator
};


class TTrue
{
public:
    explicit operator bool() const {
        return true;
    }
};

int main()
{
    cout << "Classes" << endl;

    MyClass& b = *new MyClass(); // unable to delete, memory leak
    //    MyClass *c = &b;
    //    delete c;

    MyClass* a = new MyClass();
    delete a;
    a = nullptr;

    ///Custom conversion operators allow conversions to be specified in the
    /// other direction:
    MyClass A;
    int i = A; // 5

    //................................................................
    TTrue aa, bb;
//    if (aa == bb) {} // error
    if ((bool)aa == (bool)bb) {} // allowed

    //    Bear in mind that contexts requiring a bool value, such as the
    //    condition for an if statement, count as explicit conversions.
    if (aa) {} // allowed

    return 0;
}
