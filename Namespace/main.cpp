#include <iostream>

using namespace std;

//namespace myAlias = furniture::wood; // namespace alias
//typedef my::name::MyClass MyType;

//Unlike typedef, the using
//statement also allows templates to be aliased.
//using MyType = my::name::MyClass;

//To give an example, a
//standard attribute added in C++14 is [[deprecated]]

// Mark as deprecated
[[deprecated]] void foo() {}
[[noreturn]] void f();

int main()
{
    cout << "Hello World!" << endl;
    return 0;
}
