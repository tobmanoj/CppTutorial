#include <iostream>
#include <utility>
#include <tuple>

using namespace std;

///A convenient way to return multiple values from a function is to use a tuple

/**
 * @brief getTuple
 * Non Simplified
 *
 * @return
 */
tuple<int, double, char> getTuple()
{
    return tuple<int, double, char>(5, 1.2, 'b');
}

/**
 * @brief getTupleSimplified
 * Simplified version
 */
auto getTupleSimplified()
{
    return make_tuple(5, 1.2, 'b');
}

///A feature called structured bindings was added in C++17, providing
///special language support for packing and unpacking tuple-like objects.
///With this introduction, the std::make_tuple function can be replaced
///with a list enclosed in curly brackets.
tuple<int, double, char> getTupleCpp17()
{
    return { 5, 1.2, 'b' }; // introduced in c++17
}


int main()
{
    cout << "Hello World!" << endl;

    auto mytuple = getTuple();
    cout << get<0>(mytuple) <<endl; // "5"
    cout << get<1>(mytuple) <<endl; // "1.2"
    cout << get<2>(mytuple) << endl; // "b"


    int i;
    double d;
    // Unpack tuple into variables
    std::tie(i, d, ignore) = getTupleSimplified();
    cout << i << " " << d << endl; // "5 1.2"

    ///C++17
    /// Unpacking the elements is likewise simplified and no longer
    ///requires the std::tie function. Note that the variables are now declared
    ///automatically.
    ///

    auto [ii, dd, cc] = getTupleCpp17(); // Todo Structural binding
    cout << "C++17 : " << endl;
    cout << ii << endl; // "5"
    cout << dd << endl; // "5"
    cout << cc << endl; // "5"

    return 0;
}
